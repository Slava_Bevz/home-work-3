let xhr = new XMLHttpRequest();
xhr.open('GET', './json/menu.json');
xhr.onreadystatechange = function () {
    let menu;
    if (xhr.readyState === 4 && xhr.status === 200) {
        menu = JSON.parse(xhr.responseText);
        function save(menu) {
            localStorage.menu = JSON.stringify(menu);
        }
        menu.forEach(function (elem) {
            elem.like = 0;
            if(elem.id <= 6) {
                addSharesCard(elem.productImageUrl, elem.productName, elem.ingredients, elem.price, elem.id);
            }
            save(menu);
        })
    }
};
xhr.send();
// -----------------------------------------------------------------------------------------------
function addSharesCard(imgUrl, nameCard, textCard, priceCard, idCard) { 

    const burgerCards = document.querySelector('.burgers-box'),
        sharesCards = document.querySelector('.shares-box'),
        cardItem = `
        <div class="burgers popup-link" href="#popup" data-id='${idCard}'>
            <a class="burgers__link" href="#">
                <img class="burgers__img" src="${imgUrl}">
            </a>
            <div class="burgers__info">
                <h3 class="burgers__title">${nameCard}</h3>
                <p class="burgers__text">${textCard}</p>
                <div class="burgers__btn-like">
                    <div class="burgers__price-box">
                        <span class="burgers__price">${priceCard} грн</span>
                        <button class="burgers__btn" type="button">В корзину</button>
                    </div>
                    <div class="burgers__svg-box">
                        <img class="burgers__svg" src="./img/like-none.svg"></img>
                    </div>
                </div>
            </div> 
        </div>
    `;
    burgerCards.innerHTML += cardItem;
    sharesCards.innerHTML += cardItem;
}

setTimeout(() => {

    function productInfo(info) {
        return productData = {
            id : info.dataset.id,
            name : info.querySelector('.burgers__title').innerText,
            text : info.querySelector('.burgers__text').innerText,
            img : info.querySelector('.burgers__img').getAttribute('src'),
            like : info.querySelector('.burgers__svg').getAttribute('src'),
            counter : 1,
            price : info.querySelector('.burgers__price').innerText,
        }
    }

    function modalPopup(data) {
        const popupInfo = document.querySelectorAll('.popup__content');
        data.price = Number(data.price.replace(/[^0-9]/g,""))+ ' ₴';
        const popupHTML = `
            <div class="popup__box">
                <div class="popup__img-box-like">
                    <img class="popup__img" src="${data.img}" alt="" height="266" width="286">
                    <div class="popup__add-like-box">
                        <button class="popup__info-like-box">Додати в улюблене?</button>
                        <button class="popup__like">
                            <img class="popup__like-img" src="${data.like}" alt="like">
                            <p><span>1123</span> користувача додали в улюблене</p>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal" id='${data.id}'>
                <div class="modal-info">
                    <h2>${data.name}</h2>
                    <p>${data.text}</p>
                </div>
                <div class="modal__box-btn">
                    <div class="modal__box-bar">
                        <button class="modal__plus-bar" data-action='plus' type="button">✚</button>
                        <span class="modal__text-bar" data-counter-modal>1</span>
                        <button class="modal__minus-bar" data-action='minus' type="button">━</button>
                    </div>
                    <button type="button" class="modal__price-btn" href="#">
                        <p class="modal__btn-text">Замовити</p>
                        <span class="modal__btn-price">${data.price}</span>
                    </button>
                </div>
            </div>

            <!-- <a href="#header" class="popup__close close-popup">X</a> -->
        `
        like();
        popupInfo[0].innerHTML = popupHTML;
        myClickBtnModal(data)
        modalAddProductToBasket(data);
    }

    function myClickBtnModal(data) {
        const btnPlus = document.querySelector('[data-action="plus"]'),
            btnMinus = document.querySelector('[data-action="minus"]'),
            dataCounter = document.querySelector('[data-counter-modal]'),
            btnPrice = document.querySelector('.modal__btn-price');
        btnPlus.addEventListener('click', function () {
            data.counter = dataCounter.innerText = parseInt(++dataCounter.innerText)
            btnPrice.textContent = Number(data.price.replace(/[^0-9]/g,"")) *  data.counter + ' ₴';
        })
        
        btnMinus.addEventListener('click', function () {
            if (parseInt(dataCounter.innerText) > 1) {
                data.counter = dataCounter.innerText = parseInt(--dataCounter.innerText)
                btnPrice.textContent = Number(data.price.replace(/[^0-9]/g,"")) *  data.counter + ' ₴';
            }
        })
    }


    window.addEventListener('click', (e) => {
        let counter;

        if (e.target.dataset.btn === 'plus' || e.target.dataset.btn === 'minus') {
            const itemWrapper = e.target.closest('.over-price__position-products');
            counter = itemWrapper.querySelector('[data-counter]');
            basketCounter()
        }

        if (e.target.dataset.btn === 'plus') {
            counter.innerText = ++counter.innerText;
            basketCounter()
        }

        if (e.target.dataset.btn === 'minus') {
        
            if (parseInt(counter.innerText) > 1) {
                counter.innerText = --counter.innerText;
                basketCounter()
            } else if (e.target.closest('.over-price__menu') && parseInt(counter.innerText) === 1) {
                basketCounter()
                e.target.closest('.over-price__position-products').remove();
                basketNone()
            }
            basketCounter()
        }

        if (e.target.closest('.header__basket')) {
            const blockBasket = document.querySelector('.over-price'),
                 basketBtn = document.querySelector('.header__basket');

            if (e.target.textContent === 'Кошик' && e.target.classList.length === 1) {
                basketBtn.classList.add('header__basket_active')
                blockBasket.style.display = 'block';

            } else if (e.target.textContent === 'Кошик' && e.target.classList.length === 2){
                basketBtn.classList.remove('header__basket_active')
                blockBasket.style.display = 'none';
            }
        }
    })


    function basketNone() {
        const [...basket] = document.querySelectorAll('.over-price__position-products'),
            blockBasket = document.querySelector('.over-price'),
            basketSpan = document.querySelector('.over-price__span');

        if (basket.length <= 0) {
            blockBasket.style.display = 'none';
        } else if (basket.length >= 1){
            blockBasket.style.display = 'block';
            basketSpan.style.display = 'none'
        }
    }


    function modalAddProductToBasket(data) {
        const btnModal = document.querySelector('.modal__price-btn');

        btnModal.addEventListener('click', (e) => {
            const popupActive = document.querySelector('.popup.open'),
                basket = document.querySelector('.over-price__menu'),
                itemInCard = basket.querySelector(`[data-id='${data.id}']`);
            if (itemInCard) {
                const countEl = itemInCard.querySelector('[data-counter]');
                countEl.innerText = parseInt(countEl.innerText) + parseInt(data.counter);
            } else {
                basket.innerHTML += positionProduct(data);
            }
            basketNone()
            basketCounter(data)
            popupClose(popupActive);
        })
    }

    function positionProduct(data) {
        const dataPrice = parseInt(data.price)
        return  ` 
            <div class="over-price__position-products" data-id='${data.id}' data-price='${dataPrice}'>
                <div class="over-price__menu-list">
                    <h3>${data.name}</h3>
                    <p>${data.text}</p>
                </div>
                <div class="over-price__bar">
                    <button class="over-price__plus-bar" type="button" data-btn='plus'>✚</button>
                    <span class="over-price__text-bar" data-counter>${parseInt(data.counter)}</span>
                    <button class="over-price__minus-bar" type="button" data-btn='minus'>━</button>
                </div>
                <span class="over-price__burger-price">0 ₴</span>
            </div>
        `;
    }

    function basketCounter() {
        const [...dataCounter] = document.querySelectorAll('[data-counter]'),
            totalPrice = document.querySelector('.over-price__btn-price');
        
        let totalPriceBasket = 0;

        dataCounter.forEach((e) => {

            const itemProductBasket = e.closest('.over-price__position-products'),
                dataCounterBasket = itemProductBasket.querySelector('[data-counter]').innerText
                dataPrice = parseInt(itemProductBasket.getAttribute('data-price')),
                bodyPriceBasket = itemProductBasket.querySelector('.over-price__burger-price'),
                summPrice = dataPrice * dataCounterBasket;
            
            bodyPriceBasket.innerText = `${summPrice} ₴`;
            totalPriceBasket += summPrice;
            totalPrice.innerText = `${totalPriceBasket + 50} ₴`;
        })
    }

    const [...popupLinks] = document.querySelectorAll('.popup-link');
    const body = document.querySelector('body');
    const lockPadding = document.querySelectorAll('.lock-padding');
    let unlock = true;
    const timeout = 800;

    if (popupLinks.length > 0) {
        for (let index = 0; index < popupLinks.length; index++) {
            const popupLink = popupLinks[index];
            popupLink.addEventListener('click', function (e) {
                if (e.target.className !== 'burgers__btn' && e.target.className !== 'burgers__svg') {
                    const popupName = popupLink.getAttribute('href').replace('#', '');
                    const curentPopup = document.getElementById(popupName);
                    const data = productInfo(e.target.closest('.burgers'))
                    modalPopup(data)
                    popupOpen(curentPopup);
                    e.preventDefault();
                } 

                if (e.target.className === 'burgers__btn') {
                    const basket = document.querySelector('.over-price__menu'),
                        data = productInfo(e.target.closest('.burgers')),
                        itemInCard = basket.querySelector(`[data-id='${data.id}']`);
                    if (itemInCard) {
                        const countEl = itemInCard.querySelector('[data-counter]');
                        countEl.innerText = parseInt(countEl.innerText) + parseInt(data.counter);
                    } else {
                        basket.innerHTML += positionProduct(data);
                    } 
                    basketCounter()
                    basketNone()
                }
            });
        }
    }

    const popupCloseIcon = document.querySelectorAll('.close-popup');
    if (popupCloseIcon.length > 0) {
        for (let index = 0; index < popupCloseIcon.length; index++) {
            const el = popupCloseIcon[index];
            el.addEventListener('click', function (e) {
                popupClose(el.closest('.popup'));
                e.preventDefault();
            });
        }
    }

    function popupOpen(curentPopup) {
        if (curentPopup && unlock) {
            const popupActive = document.querySelector('.popup.open');
            if (popupActive) {
                popupClose(popupActive, false);
            } else {
                bodyLock();
            }
            curentPopup.classList.add('open');
            // 
            curentPopup.addEventListener('click', function (e) {
                if (!e.target.closest('.popup__content')) {
                    popupClose(e.target.closest('.popup'));
                }
            });
        }
    }

    function popupClose(popupActive, doUnlock = true) {
        if (unlock) {
            popupActive.classList.remove('open');
            if (doUnlock) {
                bodylock();
            }
        }
    }

    function bodyLock() {
        const lockPaddingValue = window.innerWidth - document.querySelector('.container').offsetWidth - 16 + 'px';
        if (lockPadding.length > 0) {
            for (let index = 0; index < lockPadding.length; index++) {
                const el = lockPadding[index];
                el.style.paddingRight = lockPaddingValue;
            }
        }
        
        body.style.paddingRight = lockPaddingValue;
        body.classList.add('lock');
        unlock = false;
        setTimeout(function () {
            unlock = true;
        }, timeout);
    }

    function bodylock() {
        setTimeout(function () {
            if (lockPadding.length > 0) {
                for (let index = 0; index < lockPadding.length; index++) {
                    const el = lockPadding[index]; 
                    el.style.paddingRight = '0px';
                }
            }
            body.style.paddingRight = '0px';
            body.classList.remove('lock')
        }, timeout);

        unlock = false;
        setTimeout(function () {
            unlock = true;
        }, timeout);
    }
    document.addEventListener('keydown', function (e) {
        if (e.which === 27) {
            const popupActive = document.querySelector('.popup.open');
            popupClose(popupActive);
        }
    });

    (function () {
        if (!Element.prototype.closest) {
            Element.prototype.closest = function (css) {
                var nose = this;
                while (node) {
                    if (node.matches(css)) return node;
                    else node = node.parentElement;
                }
                return null;
            };
        }
        
    })();
    (function () {
        if (!Element.prototype.matches) {
            Element.prototype.matches = Element.prototype.matchesSelector || 
                Element.prototype.webkitMatchesSelector ||
                Element.prototype.mozMatchesSelector ||
                Element.prototype.msMatchesSelector;
        }
    })();

    like();

}, 100)

function like() {
    let [...imgLike] = document.querySelectorAll('.burgers__svg');
    imgLike.forEach(function (element) {
        element.addEventListener('click', function (event) {
            if (element.getAttribute('src') == './img/like.svg') {
                element.setAttribute('src', './img/like-none.svg');
                const i = JSON.parse(localStorage.menu);
                i[event.path[4].dataset.id - 1].like = 0;
                localStorage.menu = JSON.stringify(i);

            } else if (element.getAttribute('src') == './img/like-none.svg') {
                element.setAttribute('src', './img/like.svg');
                const i = JSON.parse(localStorage.menu);
                i[event.path[4].dataset.id - 1].like = 1;
                localStorage.menu = JSON.stringify(i)
            }
        })
    })
}
